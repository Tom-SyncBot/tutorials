# Configuring your discord.js project
**Please Note:** This guide is written for windows, if you are using another OS it may be different.
<br>
<br>
Firstly you will need some sort of IDE. I recommend [Visual Studio Code](https://code.visualstudio.com/).
<br>
You will also need [node.js](https://nodejs.org/en/).
<br>
## Starting the project
Firstly you will need to make a folder where your code will be stored, e.g "TomBot". Then, Navigate to the folder and then do the following; **CTRL + SHIFT + RIGHT CLICK** which then will display the menu, click **Open Powershell window here**.
<br>
Then you will need to generate a `package.json` file to do this please do the following; Go into the Powershell window you just opened and run `npm init -y` this will generate your `package.json` file which means you can now go onto installing packages.
<br>
## Installing discord.js
To install `discord.js v12` you need to navigate back to the powershell window and run `npm i discord.js`, this will add a `package-lock.json` file and a `node_modules` folder. 
<br>
That is it for bot configuration, Please move onto the next file!
