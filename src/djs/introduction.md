# discord.js Tutorial
**By `!_! Tom !_!#7945`**
<br>
<br>
Hello! Welcome to my tutorial about `discord.js v12`. At the time of writing there are currently two different versions of `discord.js` Version 11 which will no longer be supported in October 2020 and Version 12 which is the newer and up to date version of the `discord.js` libary.
<br>
In this tutorial I will cover things such as; Creating a command handler, Protecting your token, Creating Embeds and Much, Much more.
<br>
I hope you enjoy learning `discord.js`! Please note: This is still a work in progress.
