# Redis tutorial 
***by `Tmpod#1933`***

---

> Redis is an open source, in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs and geospatial indexes with radius queries. Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster.
You can run atomic operations on these types, like appending to a string; incrementing the value in a hash; pushing an element to a list; computing set intersection, union and difference; or getting the member with highest ranking in a sorted set.
In order to achieve its outstanding performance, Redis works with an in-memory dataset. Depending on your use case, you can persist it either by dumping the dataset to disk every once in a while, or by appending each command to a log. Persistence can be optionally disabled, if you just need a feature-rich, networked, in-memory cache.
Redis also supports trivial-to-setup master-slave asynchronous replication, with very fast non-blocking first synchronization, auto-reconnection with partial resynchronization on net split.  
*Extracted and abridged from [redis.io](https://redis.io/topics/introduction)*

So, basically Redis is a NoSQL database mainly for caching purposes that doesn't have complex querying (though that can be somewhat achieved through plugins), just fast in-memory storage organised in keys which can hold a wide variety of data types.

This tutorial will guide you through the process of instaling and setting up a Redis server and some practical applications in Python (more languages may come in the future).

**Let's continue over on [Installing Redis](./installing.html)!**
