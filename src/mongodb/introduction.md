# MongoDB Tutorial 
***by `Tmpod#1933`***

---

## Brief preface on NoSQL databases

> A NoSQL (originally referring to "non SQL" or "non relational") database provides a mechanism for storage and retrieval of data that is modeled in means other than the tabular relations used in relational databases. Such databases have existed since the late 1960s, but did not obtain the "NoSQL" moniker until a surge of popularity in the early 21st century, triggered by the needs of Web 2.0 companies. NoSQL databases are increasingly used in big data and real-time web applications. NoSQL systems are also sometimes called "Not only SQL" to emphasize that they may support SQL-like query languages, or sit alongside SQL database in a polyglot persistence architecture.
*Extracted and abridged from [Wikipedia](https://en.wikipedia.org/wiki/NoSQL)*

## Introduction to MongoDB

MongoDB is a cross-platform document-oriented NoSQL database. It was designed to answer to the needs of the developers of today - fast and very hassle free - having no strict schema and complex queries. It stores it's data in JSON-like documents with __schemata__. It's used a lot and it's gaining a lot of popularity.
You can checkout this amazing little repo which provides a lot of cool info and resources related to MongoDB: **https://github.com/ramnes/awesome-mongodb**.

*I'll be explaining how to use `Motor` which is an asyncio/tornado API wrapper for Python (https://github.com/mongodb/motor). More languages may come in the future*

**That's all for introductions, shall we go over to [Atlas Setup](./atlas-setup.html) where I explain you how to setup a free 512MB MongoDB cloud server? 🏃**
