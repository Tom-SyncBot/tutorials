# Introduction

This is a collection of coding related tutorials mainly aimed at Discord bot creation at the moment.

You can find the source on GitLab [**here**](https://gitlab.com/dcacademy/tutorials) *or click the little GitLab icon on the top-right corner of any page*.  
Thats where you can either contribute to or notify us of errors.

We have a [**Discord Server**](https://discord.gg/RDKfMcC) where you can find many friendly people to talk to and receive help from.

![Discord Widget](https://discordapp.com/api/v7/guilds/265828729970753537/widget.png?style=banner3)
