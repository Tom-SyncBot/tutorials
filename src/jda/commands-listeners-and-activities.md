# Commands, Listeners and Activities

Now we're going to make the commands for our bot! 

I'm just going to make a simple ping-pong command, you can see the docs here if you want insight on what you'd want to do, I highly recommend you do read them [here](https://ci.dv8tion.net/job/JDA/javadoc/)

For making other commands, you can go to the docs like it says above! and ask us in the [**`jvm-help`**](https://discordapp.com/channels/265828729970753537/651854967778443265) channel on our discord server if you've got any questions!

Now, we're gonna make a new class that extends ListenerAdapter!

For that, Im going to add a class to the `src` folder (or create a new folder and call it commands and add it in there, you also can create folders and name them based on the catgeory of the command you want to add, e.g. tictactoe belongs in fun) and name the class `PingCmd` (the `Listeneradapters` behave as events and are used to make "commands", you can use them to make your own command framework)

```java
public class PingCmd extends ListenerAdapter {
  // code
}
```

Also, this is very simple if you have some experience about java!

Now, in the class, we're going to override the `onMessageRecieved` method! This is an event that triggers when a message is sent in a guild that the bot is in! We use it like this:

```java
@Override
public void onGuildMessageRecieved(MessageRecievedEvent event)
{
  if(event.getAuthor().isBot()) return;
 // this statement checks if the Author of the message sent is a bot, and if the author of the message is a bot, it doesnt respond.

  Message msg = event.getMessage();
  // gets the message that triggered the event

  if(msg.getContentRaw().equals("!ping")) 
  // msg.getContentRaw() gets the message's
  // content with the markdown and hidden
  // characters, even the \ if it happened to
  // be after a symbol, in which discord makes
  // it disappear.
  {
    MessageChannel channel = event.getChannel();
    channel.sendMessage("Pong!').queue();
    // Important to call .queue()
  }
}
``` 

Here, you could realize that reading the comments would be very useful! Now, this code would not work with the bot if we run it, thats because we haven't added it as a listener for the bot! Now, do you remember the `JDABuilder` we used to make our bot come online? Lets go back to it!

```java
JDA jda = new JDABuilder(token).build();

jda.awaitReady();
```

Now, in this code, we add a simple line to register our listener! the line is `.addEventListener(new PingCmd())` and add it right before the `.build()` line! 

> **NOTICE:** You can replace `PingCmd` in the `.addEventListener(new PingCmd())` line to your class's (that extends `ListenerAdapter`) name followed by ()

Now, the `JDABuilder` code is:

```java
JDA jda = JDABuilder(token)
          .addEventListener(new PingCmd())
          .build();

jda.awaitReady()
```

You can add as many listeners as you want!

Which would lead us to the full code:

```java
public class Main
{
  public static void main(String[] args) throws LoginException
  {
    JDA jda = new JDABuilder(token)
          .addEventListener(new PingCmd())
           .build();
    jda.awaitReady();
  }
}

// in another class called PingCmd
public class PingCmd extends ListenerAdapter
{
  @Override
public void onMessageRecieved(MessageRecievedEvent event)
{
  if(event.getAuthor().isBot()) 
    return;
   // this statement checks if the Author of the message sent is a bot, and if the author of the message is a bot, it doesnt respond.
  if(event.isFromType(ChannelType.PRIVATE)) return;
  // this if statement checks if the message 
  // is in the DMs, and ignores it if it is, 
  // if you dont want that to happen, 
  // delete or modify this line to your needs

  Message message = event.getMessage();
  // gets the message that triggered the event
  if(msg.getContentRaw().equals("!ping"))
  // msg.getContentRaw() gets the message's
  // content with the markdown and hidden
  // characters, even the \ if it happened to
  // be after a symbol, in which discord makes
  // it disappear.
  {
    MessageChannel channel = event.getChannel();
    channel.sendMessage("Pong!').queue();
    // Important to call .queue()
  }
 }
}
```

So..... you know the cool bots, that have custom amazing playing status?
Im going to teach you all about it today!

Now, the playing status thing doesnt need a genius to make it, and its quite simple! But we need to get to our `JDABuilder` lines again! Those are pretty important if you couldn't already tell!

```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCmd())
          .build();
jda.awaitReady();
```

Now, we need to add another line above the `.build()` line again! 
See? This is getting simple and easy!

Now, here's the line to make a playing status:
`.setActivity(Activity.playing("gameNameHere"))`  
This will show: ***Playing** gameNameHere*

Now, you can change the gameNameHere to make it show anything, like this:
`.setActivity(Activity.playing("With Finn & Jake"))`  
This will display: ***Playing** with Finn & Jake*

As your bot's status! Now, this is what the JDABuilder lines would look like:

```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCmd())
          .setActivity(Activity.playing("with Finn & Jake"))
          .build();

jda.awaitReady();
```

Now, on to watching and Listening to and Streaming statuses!

You could switch `Activity.playing` with any status you want!  
For example: `Activity.listening("you")` which will show: ***Listening to** you*

> **Note:** You can also use the `setStatus` function to set the status of your bot!

E.g:
```java
JDA jda = new JDABuilder(token)
          .addEventListener(new PingCmd())
          .setActivity(Activity.playing("with Finn & Jake"))
          .setStatus(OnlineStatus.DO_NOT_DISTURB)
          .build();

jda.awaitReady();
```

And since I never explained what `jda.awaitReady();` does, it is blocking the bot and that guarantees that JDA will be completely loaded before working. This means that every line after it will only run after JDA loads.

E.g:  
If I add `System.out.println("JDA successfully loaded!");` after that line, it will only print that after JDA loads, because `jda.awaitReady();` is blocking.


> **NOTE:** This is not only limited to prints, but to any other line after `jda.awaitReady()`

---

Whenever you're ready, go into [**Embeds**](./embeds.html).
